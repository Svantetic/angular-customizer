(function(){
var app = angular.module( "CustomizeApp", ['CustomizerDirectives'] );
app.controller( 'customizeCtrl',function( $scope ){
	// inicjalizacja i 1przypisanie wartosci defaultowych
	$scope.iFrame = "#iframeContent";
	$scope.MAINCOLOR = "#25b2cc";
	$scope.styleArray= [];
	$scope.globalColor= '';
	/* selectors templates arrays */
	$scope.templateDirList = [];
	$scope.templateTypeList = [];
	$scope.templateFileList = [];
	$scope.selectedDir;
	$scope.selectedType;
	$scope.selectedFile;
	/* sample styles array */
	$scope.sampleStylesList = [{
		"name":"Default"
	}];
	$scope.selectedSampleStyle;
	$scope.buttonStyleArray = [];
	$scope.pseudoElementsArray =[];
	$scope.test = '';
	$scope.hovers =[];
	$scope.outputCss = '';
	$scope.jsonCss = '';
	$scope.intervalPause = 0;
	$scope.defaultVariables ={
		hasIframeLoaded:false,
		font:null,
		containerMaxWidth:null,
		colorPickerText:null,
		colorPickerHoverBg:null,
		colorPickerHoverLine:null,
		colorPickerBorder:null,
		headerBackgroundColor:null,
		colorPickerGlobalStyles:null
	};
	$scope.fonts = [];
	$scope.selectedFont;
	/**
	metoda odpowiedzialna za dodanie do tablicy elementów dodanie elementu , zasady i stylu danego elementu
	*/
	$scope.cssSetStyle=function(element,rule,style){
		void 0;
		if($scope.styleArray[element]===undefined){
			var rules=[];
			$scope.styleArray.length++;
			$scope.styleArray[element]=rules;
		}
		if($scope.styleArray[element][rule]===undefined)
			$scope.styleArray[element].length++;
		$scope.styleArray[element][rule]=style;
	};
	$scope.addToHoverArray = function(currItem){
	if ($scope.hovers.indexOf(currItem[0]) < 0 && currItem.prop('className').indexOf('slick') < 0){
		$scope.hovers.push(currItem[0]);
	}
}
	$scope.cssResetStyle = function(){
		void 0;
		$scope.selectedSampleStyle = 'example-styles/Default.json';
		};
	$scope.switchStyle = function(direction){
		void 0;
		if (direction==='next'){
			void 0;
			void 0;
		}
	}
	/** Metoda odpowiedzialna za wyswietlanie zmienionych wartości oraz generowanie ich, do zmiennych hubowych oraz tworzenie popup w ktorym jest wyswietlana cała zawartość */
	$scope.cssGetStyle = function(){
		jQuery("#popup").css("display","block");
		jQuery('#customizer').toggleClass('open');
		$scope.test="";
		$scope.outputCss = '';
		//console.log($scope.styleArray);
		/*if($scope.blockButtonGenerate == true){
			$scope.blockButtonGenerate = false;*/
			/* regexp do wyciagania linku z obiektu fonta*/
			var selectedFontURL = $scope.selectedFont.link.match(/http.*\d/i)[0];
			var selectedFontName = $scope.selectedFont.font;
			//console.log(($scope.selectedFont.link).match(/http.*\d/i)[0]);
			$scope.test = '@import url("'+selectedFontURL+'");<br>';
			$scope.test += '{% set fontFamily = '+selectedFontName+' %} </br>';
				for(var style in $scope.styleArray){
					//console.log('style array:'+$scope.styleArray);
					$scope.test += "{% set "+style+" = ";
					//$scope.outputCss += ''+ style + ' { <br>';
						for(var value in $scope.styleArray[style]){
							$scope.test += "'"+($scope.styleArray[style][value])+"' %} <br>";
							/*console.log(value);
							console.log($scope.styleArray[style][value]);*/
						}
				}
			/*
			 * funkcja odpowiedzialna za appendowanie do popupu elementów z tablicy zawierajacej te elementy, do ktorych
			 * dodawany jest hover.
			 */
			var generatedPara = jQuery('#generatedCss');
			jQuery("#generatedValues").html($scope.test);
			$scope.outputCss = jQuery('#colorPickerGlobalStyles').css('background-color');
			$scope.hovers.forEach( function(element, index) {
				if (element.id){
					if (generatedPara.text().indexOf('#'+element.id) < 0) {
						generatedPara.append('#'+element.id+':hover { </br>');
						generatedPara.append('	background-color: white !important; </br>');
						generatedPara.append('	color: #333; </br>');
						generatedPara.append('} </br>');
					}
				}
				else if (element.className.indexOf('fa-') > 0) {
						if (generatedPara.text().indexOf('.'+element.className.replace(/ /g, '.')) < 0) {
						generatedPara.append('.'+element.className.replace(/ /g, '.')+':hover { </br>');
						generatedPara.append('	background-color: white !important; </br>');
						generatedPara.append('	color: #333; </br>');
						generatedPara.append(' box-shadow: 5px 5px 0px rgba('+$scope.globalColor.r+', '+$scope.globalColor.g+', '+$scope.globalColor.b+', 0.4)');
						generatedPara.append('</br>} </br>');
					}
				}
				else {
					if (generatedPara.text().indexOf('.'+element.className.replace(/ /g, '.')) < 0) {
						generatedPara.append('.'+element.className.replace(/ /g, '.')+':hover { </br>');
						generatedPara.append('	background-color: white !important; </br>');
						generatedPara.append('	color: #333; </br>');
						generatedPara.append('} </br>');
					}
				}
				//console.log(element);
			});
			/*jQuery("#popup #generatedCss p").html(".cta_button : { </br> background-color: "+$scope.outputCss+"; </br> }</br>");
			jQuery("#popup #generatedCss p").html(jQuery("#popup #generatedCss p").html()+".cta_button:hover { </br> background-color: white; </br> color: #333; </br>}");
			console.log($scope.hovers);
			//console.log(jQuery("#iframeContent").contents().find('head style#pseudoStyles').text());*/
		//}
	}
	/* metoda odpowiedzialna za zamkniecie popup'a popup ma w css przypisana wartosc na display none */
	$scope.closeTextContent = function(){
		jQuery("#popup").css("display","none");
		jQuery('#customizer').toggleClass('open');
	}
	/*
		metoda odpowiedzialna za wyszukanie pseudoelementów w kodzie takich jak before i after oraz przypisanie ich do tablicy
		i zmiane koloru atrubutów css takich jak : color , background na samym koncu nastepuje podmiana defaultowych stylów templatki na nowo wygenerowany styl dla pseudo elementow
	*/
	$scope.buttonAction = function(newStyle){
		function addPseudoElementToArray(element,rule,style){
			if($scope.pseudoElementsArray[element] == undefined){
				var rules =[];
				$scope.pseudoElementsArray.length++;
				$scope.pseudoElementsArray[element]= rules;
			}
			if($scope.pseudoElementsArray[element][rule]== undefined)
				$scope.pseudoElementsArray[element].length++;
			$scope.pseudoElementsArray[element][rule] = style;
		}
		for(var singleElement in $scope.jsonCss.children){
			if(singleElement.search(":before")>-1){
				for(var x in $scope.jsonCss.children[singleElement]['attributes']){
					if(x == 'color'){
						if($scope.MAINCOLOR == $scope.jsonCss.children[singleElement]['attributes'][x]){
							//console.log(singleElement+" "+x+" "+$scope.jsonCss.children[singleElement]['attributes'][x]);
							addPseudoElementToArray(singleElement,x,newStyle);
						}
					}
					if(x == 'background'){
						//addPseudoElementToArray(singleElement,x,'#bd45df');
					}
					if (x == 'color') {
						addPseudoElementToArray(singleElement, x, newStyle);
					}
					if (x == 'border-top') {
						void 0;
					}
				}
			}
			if(singleElement.search(":after")>-1){
				for(var x in $scope.jsonCss.children[singleElement]['attributes']){
					if (singleElement == '.hs-menu-wrapper.hs-menu-flow-horizontal>ul>li.hs-menu-depth-1>a:after'){
						//console.log($scope.jsonCss.children[singleElement]['attributes']);
						//console.log('atrybuty: ', x);
					}
					if(x == 'color'){
						//console.log('dodaj after');
						//console.log(" Element: "+singleElement+" Rule: "+x+" Value: "+$scope.jsonCss.children[singleElement]['attributes'][x]);
						addPseudoElementToArray(singleElement,x,newStyle);
					}
					if(x == 'background'){
						//console.log('jest box-shadow i after');
						//console.log(singleElement);
						addPseudoElementToArray(singleElement,x,newStyle+' !important');
					}
					if (x == 'box-shadow') {
						addPseudoElementToArray(singleElement,x,'0 0 0 3px '+newStyle);
					}
				}
			}
			/*if (singleElement.search(":hover")>-1) {
				//console.log($scope.jsonCss.children[singleElement]);
				for (var x in $scope.jsonCss.children[singleElement]['attributes']) {
					if (x == 'border-bottom') {
						addPseudoElementToArray(singleElement, 'border-bottom-color', newStyle);
					}
					if (x == 'background-color' || x == 'background') {
						//console.log('bg color hover test');
						addPseudoElementToArray(singleElement, 'background-color', '#fff !important');
						addPseudoElementToArray(singleElement, 'background', '#fff !important');
					}
				}
			}*/
		}
		var allStyle='';
		for(var style in $scope.pseudoElementsArray){
			for(var inner in $scope.pseudoElementsArray[style]){
				if (style!='.hs-item-has-children:hover>ul.hs-menu-children-wrapper li.hs-menu-item>a') {
					allStyle+= style+' {'+inner+" : "+$scope.pseudoElementsArray[style][inner]+";"+"}";
				}
			}
		}
		jQuery($scope.iFrame).contents().find("style[attr='customStyle']").text("");
		jQuery($scope.iFrame).contents().find("style[attr='customStyle']").text(allStyle);
	}
	/**
		funkcja przyjmuje jeden parametr element ktory przyjmuje wartosc kolor i jest sprawdzany czy jego wartosc nie wynosi "rgba(0, 0, 0, 0)" lub undefined,
		jeżli jego wartosc jest rowna w/w wartości wtedy zwraca kolor "#ffffff"(biały)
	*/
	$scope.checkColor= function(element){
				if(element == "rgba(0, 0, 0, 0)"){
					return "#ffffff";
				}
				if(element == undefined){
					return "#ffffff";
				}
				return element;
	}
	/**
	funkcja odpowiedzialna za pobieranie fontów metodą Ajax
	*/
	/** Główna funkcja w ktorej sa przypisywane deafultowe wartosci,
		wywoływana jest funkcja odpowiedzialna za pobieranie fontow z pliku googleFonts.json ktory znajduje sie w katalogu fonts,
		oraz ajaxem pobierane sa wszystkie style do danej templaty
		w tej funkcji znajduje sie rownież set interval ktory jest odpowiedzialny ze wszystkie buttony ktore sa ładowane asynchonicznie
	*/
	$scope.controlsLoadedCallBack = function(){
		void 0;
	};
	$scope.iframeLoadedCallBack = function(){
		$scope.defaultVariables.font = {"font": jQuery('#iframeContent').contents().find('body').css('font-family').slice(0, jQuery('#iframeContent').contents().find('body').css('font-family').indexOf(',')).replace(/\'/g, '')};
		$scope.defaultVariables.containerMaxWidth = jQuery($scope.iFrame).contents().find(".container").css("maxWidth");
		$scope.defaultVariables.colorPickerText = jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("color");
		$scope.defaultVariables.colorPickerHoverBg = jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li ul").css("background");
		$scope.defaultVariables.colorPickerHoverLine = jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li ul li a").css("background");
		$scope.defaultVariables.colorPickerBorder = jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("border-bottom-color");
		$scope.defaultVariables.headerBackgroundColor = jQuery($scope.iFrame).contents().find("#main-header").css("background");
		$scope.defaultVariables.hasIframeLoaded = true;
		$scope.$apply();
		/*$.getJSON('php/list-templates.php',
			{
				'dirname': 'Templates'
			},
			function(json, textStatus) {
				$.each(json, function(key, value){
					$scope.templatesList.push({
						"templateURL" : value,
						"templateName" : key
					});
				});
				console.log($scope.templatesList);
				console.log('select options loaded');
		});
		jQuery('#selectTemplate').on('change', function(){
			var newTemplateURL = jQuery('#selectTemplate option:selected').text();
			jQuery("iframe#iframeContent").attr('src', newTemplateURL);
		});*/
		jQuery(".element-container").css("display","block");
		$.get("sites/style.css",function(data){
			jQuery("<style attr='customStyle'></style>").appendTo(jQuery($scope.iFrame).contents().find("head"));
			jQuery($scope.iFrame).contents().find("style[attr='customStyle']").text(data);
			//console.log(jQuery($scope.iFrame).contents().find("style[attr='customStyle']").text());
			$scope.jsonCss = CSSJSON.toJSON(jQuery($scope.iFrame).contents().find("style[attr='customStyle']").text());
		});
		$scope.defaultVariables.containerMaxWidth = jQuery($scope.iFrame).contents().find(".container").css("maxWidth");
		$scope.intervalPause=setInterval(function(){
			if($("#iframeContent").contents().find(".hs_cos_wrapper_type_cta").length == $("#iframeContent").contents().find(".cta_button").length){
				clearInterval($scope.intervalPause);
				setTimeout(function(){
					$("#iframeContent").contents().find("body *").each(function(){
						if($(this).css("color") == "rgb(37, 178, 204)" || $(this).css("color") == "#25b2cc" ){
							$(this).attr("global-color",$scope.MAINCOLOR);
						}
						if($(this).css("backgroundColor") == "rgb(37, 178, 204)" || $(this).css("backgroundColor") == "#25b2cc" ){
							$(this).attr("global-background-color",$scope.MAINCOLOR);
						}
					});
				},100);
			}
		},50);
 }
});
})();