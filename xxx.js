app.directive('fontSelectedList',function(){
	return{
		restrict: 'A',
		templateUrl:'elements/font-selected-list.html',
		replace: true,
		controller:function($scope){
			function appendStyleOfFont(fontStyle){
				jQuery("#iframeContent").contents().find("head").append("<style> .change-font{ font-family:"+fontStyle+"!important;}</style>");
				jQuery("#iframeContent").contents().find("body").addClass("change-font");
			}
			var singleLiList =[];
			var linkToHead = [];
			var checkLoaded = false;	
			jQuery(".select span").click(function(e){
				e.stopPropagation();
				jQuery(".select ul").fadeIn();
					if(checkLoaded == false){
						for(var singleFont in $scope.fonts){
							console.log($scope.fonts[singleFont]["font-family"]);
							singleLiList.push("<li><span style=\"font-family:"+$scope.fonts[singleFont]["font-family"]+";\">"+$scope.fonts[singleFont]["font"]+"</span></li>");
							linkToHead.push($scope.fonts[singleFont]["link"]);
						}						
						jQuery(".select ul").append(singleLiList);
						jQuery("#iframeContent").contents().find("head").append(linkToHead);
					}
						checkLoaded = true;
			});
			jQuery(".select ul").click(function(e){
				//console.log(jQuery(e.target).css("font-family"));
				appendStyleOfFont(jQuery(e.target).css("font-family"));
			});
			jQuery(document).on('click',function(){
				jQuery(".select ul").fadeOut();
			});
		}
	}
});     