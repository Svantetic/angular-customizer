(function() {
    app = angular.module("CustomizerDirectives", []);
    app.directive("iframeOnload", function() {
        return {
            scope: {
                callBack: '&iframeOnload'
            },
            link: function(scope, element, attrs) {
                element.on('load', function() {
                    return scope.callBack();
                })
            }
        }
    });
    app.directive("controlsOnload", function() {
        return {
            scope: {
                callBack: '&controlsOnload'
            },
            link: function(scope, element, attrs) {
                element.on('load', function() {
                    return scope.callBack();
                })
            }
        }
    });
    /*
Tworzy dyrektywe odpowiedzialną za suwak w menu,
ktory zmiena szerokosc kontenera (.container)
*/
    app.directive('inputRangeWidth', function() {
        return {
            restrict: 'A',
            templateUrl: 'elements/input-range-width-of-content.html',
            controller: function($scope) {
                var sliderHistory = [];
                $scope.inputRangeValue = 1140;
                var min = jQuery("#sliderRange").attr('min');
                var max = jQuery("#sliderRange").attr('max');
                min = parseInt(min);
                jQuery('#sliderRange').mousedown(function(event) {
                    if (sliderHistory.length > 2) {
                        sliderHistory.pop();
                    }
                    sliderHistory.unshift(jQuery(this).val());
                });
                jQuery('#sliderRangeUndo').click(function(event) {
                    console.log('klikam');
                    console.log('history: ' + sliderHistory[0]);
                    console.log('scope ' + $scope.inputRangeValue);
                    jQuery('#sliderRange').val(sliderHistory[0]);
                    $scope.$apply(function() {
                        if (sliderHistory.length > 0) {}
                        $scope.inputRangeValue = sliderHistory.shift();
                    });
                });
                max = parseInt(max);
                $scope.$watch('inputRangeValue', function(oldValue, newValue) {
                    $scope.inputRangeValue = parseInt($scope.inputRangeValue);
                    if ($scope.inputRangeValue < min) {
                        console.log($scope.inputRangeValue + " : mniejszy : " + min);
                    } else {
                        jQuery($scope.iFrame).contents().find(".container").css("maxWidth", $scope.inputRangeValue + "px");
                        $scope.cssSetStyle('Container', "maxWidth", $scope.inputRangeValue + 'px');
                    }
                }, true);
            }
        }
    });
    /*
Tworzy dyrektywe odpowiedzialną za 'zmiane css headera z position:fixed na static'
*/
    app.directive('selectOfStickHeader', function() {
        return {
            restrict: 'A',
            templateUrl: 'elements/select-of-stick-header.html',
            controller: function($scope) {
                jQuery($scope.iFrame).load(function() {
                    if (paddingElement == (-1)) {
                        paddingElement = jQuery($scope.iFrame).contents().find(".body-container-wrapper").css("padding-top");
                    }
                });
                $scope.selectedOptionOfHeader = 'fixed';
                var paddingElement = (-1);
                $scope.$watch('selectedOptionOfHeader', function() {
                    var iFrameHeader = jQuery($scope.iFrame).contents().find("#main-header");
                    console.log($scope.selectedOptionOfHeader);
                    //console.log(mainContainer);
                    if ($scope.selectedOptionOfHeader == "static") {

                        iFrameHeader.css("position", 'relative');
                        iFrameHeader.css('z-index', '25px');
                        jQuery('#iframeContent').contents().find('.body-container-wrapper').css('padding-top', '24px');
                        //var headerHeight = jQuery("#iframeContent").content().find('.header-container-wrapper').css('height');
                        //$scope.cssSetStyle('StickHeader', "position", $scope.selectedOptionOfHeader);
                    } else if ($scope.selectedOptionOfHeader == "fixed") {
                        iFrameHeader.css('position', 'fixed');
                        // mainContainer.css('padding-top', 25+'px');
                        jQuery('#iframeContent').contents().find('.body-container-wrapper').css('padding-top', ' 101px');
                        //console.log('header height'+headerHeight); 
                        //jQuery($scope.iFrame).contents().find(".header-container-wrapper").css("position", 'static');
                        //jQuery($scope.iFrame).contents().find(".header-container-wrapper").css("position", 'static').css('z-index','10');
                        //jQuery($scope.iFrame).contents().find(".body-container-wrapper").css("padding-top", paddingElement - 1);
                        //$scope.cssSetStyle('StickHeader', "position", $scope.selectedOptionOfHeader);
                        //jQuery($scope.iFrame).contents().find(".body-container-wrapper").css("padding-top", headerHeight+'px');
                    }
                });
            }
        }
    });
    app.directive('ngElementReady', [
        function() {
            return {
                priority: Number.MIN_SAFE_INTEGER, // execute last, after all other directives if any.
                restrict: "A",
                link: function($scope, $element, $attributes) {
                    $scope.$eval($attributes.ngElementReady); // execute the expression in the attribute.
                }
            };
        }
    ]);
    /* dyrektywa odpowiadajaca za wczytanie przykladowych styli */
    app.directive('samplestylesList', function() {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: "elements/samplestyles-list.html",
            controller: function($scope, $http) {
                var currIndex = 0;
                var url = 'php/list-styles.php';
                $http({
                    method: 'GET',
                    url: url
                }).then(function(response) {
                    //console.log(response.data);
                    $.each(response.data, function(key, value) {
                        $http({
                            method: 'GET',
                            url: value
                        }).then(function(response) {
                            //console.log(response.data);
                            $scope.sampleStylesList.push(response.data);
                        });
                    });
                    var sampleStylesContainer = jQuery('#sample-styles-list');
                    sampleStylesContainer.click(function() {
                        jQuery(this).toggleClass('open');
                        sampleStylesContainer.find('.fa').toggleClass('fa-rotate-180');
                    });
                });
                $scope.switchStyle = function(direction) {
                    if (currIndex < $scope.sampleStylesList.length && direction == 'next') {
                        currIndex++;
                        console.log('currIndex: ' + currIndex);
                        $scope.updateColorPicker(currIndex);
                    }
                    if (currIndex > 1 && direction == 'prev') {
                        currIndex--;
                        console.log('currIndex: ' + currIndex);
                        $scope.updateColorPicker(currIndex);
                    }
                    if (currIndex > 0) {
                        jQuery('#prevStyle').prop('disabled', false);
                    }
                    if (currIndex === 0) {
                        jQuery('#prevStyle').prop('disabled', true);
                    }
                }
                $scope.updateColorPicker = function(index) {
                        if (index > 0) {
                            //console.log('index w pierwszej+' + index);
                            updateColorPickers($scope.sampleStylesList[index]);
                            currIndex = index;
                            $scope.sampleStylesList.shift();
                            $scope.sampleStylesList.unshift($scope.sampleStylesList[index - 1]);
                            if (jQuery('#sample-styles-list li:first-child .style-index').length > 0) {
                                jQuery('#sample-styles-list li:first-child .style-index').remove();
                            }
                            jQuery('#sample-styles-list li:first-child').prepend('<span class="style-index" style="float:left;">#' + index + '</span>');
                            //jQuery('#sample-styles-list li:first-child>.colorcode').append("#"+arg);

                            //$scope.sampleStylesList.splice($scope.sampleStylesList.indexOf($scope.sampleStylesList[arg]), 1);
                        }
                    }
                    // $scope.$watch('selectedSampleStyle', function(oldValue, newValue) {
                    // 	var sampleUrl = oldValue;
                    // 	$http({
                    // 		method: 'GET',
                    // 		url: sampleUrl
                    // 		}).then(function(response){
                    // 			updateColorPicker(response.data);
                    // 		}, function(error){
                    // 			console.log('error...');
                    // 			console.log(error);
                    // 		});
                    // });
            }
        }
    });
    /* funkcja odpowiadajaca za updatowanie colorpickera z wczytywanego stylu .json */
    function updateColorPickers(sampleStyle) {
        console.log('jest wywolywana dtuga funcjia');
        var styleButtons = jQuery('.colorpicker');
        var i = 0;
        styleButtons.each(function(index, el) {;
            styleButtons[index] = jQuery(this).find('.colorpicker_hex input');
        });
        $.each(sampleStyle.colors, function(index, val) {
            var input = styleButtons[i];
            input.val(val).change();
            i++;
        });
    }
    /* dyrektywa odpowiadajaca za wczytanie templatow */
    app.directive('templatesList', function() {
        return {
            restrict: 'A',
            replace: true,
            priority: -100000,
            templateUrl: "elements/templates-list.html",
            controller: function($scope, $http) {
                /* getting list of template names */
                var url = 'php/list-templates.php';
                $http({
                    method: 'GET',
                    url: url,
                    params: {
                        dirname: 'Templates',
                        type: 'directory'
                    },
                }).then(function(response) {
                    $.each(response.data, function(key, value) {
                        $scope.templateDirList.push({
                            'key': key,
                            'value': value
                        })
                    })
                }, function(response) {
                    console.log('error' + response);
                });
                jQuery('#templateDirSelect').on('change', function() {
                    $scope.$watch('selectedDir', function(newValue, oldValue) {
                        jQuery('#templateTypeSelect').prop('disabled', false);
                        var selectedDir = newValue;
                        $http({
                            method: 'GET',
                            url: url,
                            params: {
                                dirname: selectedDir,
                                type: 'templateType'
                            },
                        }).then(function(response) {
                            console.log(response.data);
                            $scope.templateTypeList = [];
                            jQuery('#templateTypeSelect').prop('disabled', false);
                            $.each(response.data, function(key, value) {
                                $scope.templateTypeList.push({
                                    'key': key,
                                    'value': value
                                })
                            })
                        })
                    });
                })
                jQuery('#templateTypeSelect').on('change', function() {
                    $scope.$watch('selectedType', function(newValue, oldValue) {
                        jQuery('#templateFileSelect').prop('disabled', false);
                        var selectedDir = $scope.selectedDir;
                        var selectedType = newValue;
                        var dirname = selectedDir + '/' + selectedType;
                        console.log(dirname);
                        $http({
                            method: 'GET',
                            url: url,
                            params: {
                                dirname: dirname,
                                type: 'file'
                            },
                        }).then(function(response) {
                            console.log(response.data);
                            $scope.templateFileList = [];
                            jQuery('#templateFileSelect').prop('disabled', false);
                            $.each(response.data, function(key, value) {
                                $scope.templateFileList.push({
                                    'key': key,
                                    'value': value
                                })
                            })
                        })
                    })
                });
                jQuery('#templateFileSelect').on('change', function() {
                    $scope.$watch('selectedFile', function(newValue, oldValue) {
                        var file = 'Templates/' + $scope.selectedDir + '/' + $scope.selectedType + '/' + newValue;
                        console.log(file);
                        jQuery('#iframeContent').attr('src', file);
                    })
                });
            }
        }
    });
    /*var url = 'php/list-templates.php';
		$.getJSON(url, {
			type: 'directory'
		},
		function (json, status) {
			$.each(json, function(key, value){
				$scope.templateDirList.push({
					"templateDirList" : value
				});
			});
		});
		/*$.getJSON('php/list-templates.php',
			{
				'dirname': 'Templates'
			},
			function(json, textStatus) {
				$.each(json, function(key, value){
					$scope.templatesList.push({
						"templateURL" : value,
						"templateName" : key
					});
				});
				console.log($scope.templatesList);
				console.log('test');
		});
		jQuery('#selectTemplate').on('change', function(){
			var newTemplateURL = jQuery('#selectTemplate option:selected').text();
			jQuery("iframe#iframeContent").attr('src', newTemplateURL);
		});
	}
}});*/
    /**
Dyrektywa odpowiedzialna za customizowanie paska menu oraz samego menu, rzeczy ktore mozna customizowac to:
app.di
 */
    app.directive('customizeHeaderMenu', function() {
        return {
            restrict: 'A',
            templateUrl: "elements/customize-header-menu.html",
            controller: function($scope) {
                //pobierz defaultowe wartości
                jQuery($scope.iFrame).load(function() {
                    jQuery('#colorPickerText').css("backgroundColor", $scope.checkColor(jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("color")));
                    jQuery('#colorPickerHoverBg').css('backgroundColor', $scope.checkColor(jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li ul").css("backgroundColor")));
                    jQuery('#colorPickerHoverLine').css('backgroundColor', $scope.checkColor(jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li ul li a").css("backgroundColor")));
                    jQuery('#colorPickerBorder').css('backgroundColor', $scope.checkColor(jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("border-bottom-color")));
                    $scope.cssSetStyle('menuChangeColorOfFont', "color", jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("color"));
                    $scope.cssSetStyle('backgroundColorAfterHover', "background", jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("background-color"));
                    $scope.cssSetStyle('#main-header .main-menu ul li ul li a', "background", jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("background-color"));
                    $scope.cssSetStyle('#main-header .main-menu ul li ul', "border-bottom-color", jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("border-bottom-color"));
                    $scope.cssSetStyle('#main-header .main-menu ul li a:hover', "border-bottom-color", jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("border-bottom-color"));
                });
                jQuery('#colorPickerText').ColorPicker({
                    onShow: function(colpkr) {
                        $(colpkr).fadeIn(300);
                        return false;
                    },
                    onHide: function(colpkr) {
                        $(colpkr).fadeOut(300);
                        return false;
                    },
                    onChange: function(hsb, hex, rgb) {
                        var hexadecimal = "#" + hex;
                        jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("color", hexadecimal);
                        jQuery("#colorPickerText").css("background", hexadecimal);
                        $scope.cssSetStyle('menuChangeColorOfFont', "color", hexadecimal);
                    }
                });
                jQuery('#colorPickerDropdownColor').ColorPicker({
                    onShow: function(colpkr) {
                        $(colpkr).fadeIn(300);
                        return false;
                    },
                    onHide: function(colpkr) {
                        $(colpkr).fadeOut(300);
                        return false;
                    },
                    onChange: function(hsb, hex, rgb) {
                        var hexadecimal = "#" + hex;
                        var headerList = jQuery($scope.iFrame).contents().find("#main-header ul .hs-menu-item ul");
                        document.styleSheets[0].addRule(headerList.selector + ':before', 'color:green');
                        document.styleSheets[0].insertRule(headerList.selector + ':before {color: green }', 0);
                        headerList.css("background", hexadecimal);
                        jQuery("#colorPickerDropdownColor").css("background", hexadecimal);
                        $scope.cssSetStyle('backgroundColorAfterHover', "background", hexadecimal);
                    }
                });
                jQuery('#colorPickerHoverLine').ColorPicker({
                    onShow: function(colpkr) {
                        $(colpkr).fadeIn(300);
                        return false;
                    },
                    onHide: function(colpkr) {
                        $(colpkr).fadeOut(300);
                        return false;
                    },
                    onChange: function(hsb, hex, rgb) {
                        var oldHex = jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li ul li a").css("background");
                        var hexadecimal = "#" + hex;
                        jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li ul li a").hover(function() {
                            jQuery(this).css("background", hexadecimal);
                        }, function() {
                            jQuery(this).css("background", oldHex);
                        });
                        jQuery("#colorPickerHoverLine").css("background", hexadecimal);
                        $scope.cssSetStyle('backgroundMenuElement', "background", hexadecimal);
                    }
                });
                jQuery('#colorPickerBorder').ColorPicker({
                    onShow: function(colpkr) {
                        $(colpkr).fadeIn(300);
                        return false;
                    },
                    onHide: function(colpkr) {
                        $(colpkr).fadeOut(300);
                        return false;
                    },
                    onChange: function(hsb, hex, rgb) {
                        var hexadecimal = "#" + hex;
                        var oldHex = jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").css("border-bottom-color");
                        jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li ul").css("border-bottom-color", hexadecimal);
                        jQuery($scope.iFrame).contents().find("#main-header .main-menu ul li a").hover(function() {
                                $(this).css("border-bottom-color", hexadecimal);
                            },
                            function() {
                                $(this).css("border-bottom-color", oldHex);
                            });
                        jQuery("#colorPickerBorder").css("background", hexadecimal);
                        $scope.cssSetStyle('borderElementColor', "border-bottom-color", hexadecimal);
                        $scope.cssSetStyle('borderAfterHover', "border-bottom-color", hexadecimal);
                    }
                });
            }
        }
    });
    app.directive('headerBackgroundColor', function() {
        return {
            restrict: 'A',
            templateUrl: "elements/header-background-color.html",
            controller: function($scope) {
                jQuery('#headerBackgroundColor').css('backgroundColor', $scope.checkColor(jQuery($scope.iFrame).contents().find("#main-header").css("background")));
                jQuery('#headerBackgroundColor').ColorPicker({
                    onShow: function(colpkr) {
                        $(colpkr).fadeIn(300);
                        return false;
                    },
                    onHide: function(colpkr) {
                        $(colpkr).fadeOut(300);
                        return false;
                    },
                    onChange: function(hsb, hex, rgb) {
                        var hexadecimal = "#" + hex;
                        jQuery($scope.iFrame).contents().find("#main-header").css("background", hexadecimal);
                        jQuery("#headerBackgroundColor").css("background", hexadecimal);
                        $scope.cssSetStyle('headerColor', "background", hexadecimal);
                    }
                });
            }
        }
    });
    app.directive('buttonsCustomize', function() {
            return {
                restrict: 'A',
                templateUrl: 'elements/button-customize.html',
                replace: true,
                controller: function($scope) {
                    var cta;
                    var iframe = document.getElementById('iframeContent');
                    var iframeDocument = iframe.contentDocument || iframe.contentWindow.document;

                    function css(rule, state) {
                        var sheets = iframeDocument.styleSheets;
                        var slen = sheets.length;
                        for (var i = 0; i < slen; i++) {
                            var rules = sheets[i].cssRules;
                            if (rules != null) {
                                var rlen = rules.length;
                                for (var j = 0; j < rlen; j++) {
                                    if (state == "") {
                                        if (rules[j].selectorText.search("a#cta_button_") > -1) {
                                            return rules[j].style;
                                            //console.log(rules[j].style)
                                            //$scope.outputCss += ''+rules[j].style+'<br>';
                                        }
                                    } else {
                                        if (rules[j].selectorText.search("a#cta_button_") > -1 && rules[j].selectorText.search(state) > -1) {
                                            return rules[j].style;
                                            //console.log(rules[i].style)
                                            //$scope.outputCss += ''rules[j].style+'<br>';
                                        }
                                    }
                                }
                            }
                        }
                    }

                    function clearCss(css) {
                        var cssStyles = [];
                        var temp = [];
                        temp = css['cssText'].split(";");
                        var temp2;
                        for (var x in temp) {
                            temp2 = temp[x].split(":");
                            if (temp2[1] != undefined) {
                                cssStyles[temp2[0]] = temp2[1];
                            }
                        }
                    }
                    setTimeout(function() {
                        var style = css("[data-hs-cos-type='cta'] a", "");
                        cta = jQuery($scope.iFrame).contents().find("[data-hs-cos-type='cta'] a");
                        $.each(jQuery($scope.iFrame).contents().find("[data-hs-cos-type='cta'] a"), function(i, e) {
                            if ($(e).find("img").length > 0) {
                                cta[i] = null;
                            }
                        });
                        clearCss(style);
                    }, 3000);
                    jQuery('#colorPickerButtonBG').ColorPicker({
                        onShow: function(colpkr) {
                            $(colpkr).fadeIn(500);
                            return false;
                        },
                        onHide: function(colpkr) {
                            $(colpkr).fadeOut(500);
                            return false;
                        },
                        onChange: function(hsb, hex, rgb) {
                            var RGB = rgb;
                            jQuery($scope.iFrame).contents().find("[data-hs-cos-type='cta'] a").find("img");
                            jQuery("#colorPickerButtonBG").css("background", "#" + hex);
                            cta.css('background', "#" + hex);
                        }
                    });
                }
            }
        }),
        app.directive('generateCss', function() {
            return {
                restrict: 'A',
                templateUrl: 'elements/generate-css.html',
                replace: true,
                controller: function() {}
            }
        }),
        app.directive('globalStyleCss', function() {
            return {
                restrict: 'A',
                templateUrl: 'elements/global-style-css.html',
                replace: true,
                priority: -1000000,
                controller: function($scope) {
                    $scope.cssSetStyle('globalColor', "background", $scope.MAINCOLOR);
                    jQuery('#colorPickerGlobalStyles').css('backgroundColor', $scope.checkColor($scope.MAINCOLOR));
                    var appended = false;
                    var pseudoStyles;
                    jQuery('#colorPickerGlobalStyles').ColorPicker({
                        onShow: function(colpkr) {
                            $(colpkr).fadeIn(300);
                            return false;
                        },
                        onHide: function(colpkr) {
                            $(colpkr).fadeOut(300);
                            return false;
                        },
                        onClick: function(colpkr) {
                            console.log('clicked');
                        },
                        onSubmit: function(hsb, hex, rgb) {
                            console.log('submitted');
                            console.log('hsb', 'hex', 'rgb');
                        },
                        onChange: function(hsb, hex, rgb) {
                            $scope.cssSetStyle('globalColor', "background", '#' + hex);
                            jQuery('#colorPickerGlobalStyles').css('backgroundColor', $scope.checkColor(hex));
                            $scope.globalColor = rgb;
                            $scope.buttonAction("#" + hex);
                            if (!jQuery('#iframeContent').contents().find('head style#pseudoStyles').length) {
                                jQuery('#iframeContent').contents().find('head').append('<style id="pseudoStyles">');
                                pseudoStyles = jQuery('#iframeContent').contents().find('head style#pseudoStyles');
                            }
                            jQuery("#iframeContent").contents().find("body .body-container-wrapper *").each(function() {
                                var currItem = jQuery(this);
                                if (currItem.prop('className').indexOf('fa-') > -1 && currItem.attr('global-color')) {
                                    var boxShadow = 'box-shadow: 0 0 0 10px rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', .4) !important';
                                    pseudoStyles.append(currItem.prop('tagName').toLowerCase() + '.' + currItem.prop('class').replace(/ /g, '.') + ':hover { box-shadow: 0 0 0 10px rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', .4) !important; color: white !important; background-color: #' + hex + ' !important; }');
                                    $scope.addToHoverArray(currItem);
                                }
                                if (currItem.prop('tagName') == 'A') {
                                    if (currItem.parents()[1].className !== 'active-branch')
                                        pseudoStyles.append('.' + currItem.parents()[1].className + ' a:hover { background-color: white !important; color:#333 !important; }');
                                } else if (currItem.prop('tagName') == 'BUTTON' || currItem.prop('class').indexOf('button') > -1 ||
                                    currItem.prop('type') == 'button' || currItem.prop('type') == 'submit') {
                                    if (currItem.prop('id')) {
                                        pseudoStyles.append(currItem.prop('tagName').toLowerCase() + '#' + currItem.prop('id') + ':hover { background: white !important; color: #333 !important; }');
                                        $scope.addToHoverArray(currItem);
                                        //console.log('appended: '+appended);
                                    } else {
                                        if (pseudoStyles.text().indexOf(currItem.prop('tagName').toLowerCase() + '.' + currItem.prop('class').replace(/ /g, '.') + ':hover') < 0) {
                                            pseudoStyles.append(currItem.prop('tagName').toLowerCase() + '.' + currItem.prop('class').replace(/ /g, '.') + ':hover {\
										background: white !important; color: #333 !important;	\
										})');
                                            $scope.addToHoverArray(currItem);
                                        }
                                    }
                                }
                                if (currItem.attr("global-color") == $scope.MAINCOLOR) {
                                    currItem.css("color", "#" + hex);
                                    var currentTagName = currItem.prop('tagName');
                                    document.styleSheets[0].addRule(currentTagName + ':after', 'content: box-shadow-color:"' + hex + '";');
                                }
                                if (currItem.attr("global-background-color") == $scope.MAINCOLOR) {
                                    currItem.css("background-color", "#" + hex);
                                    var currentTagName = currItem.prop('tagName');
                                    currItem.css('border-color', '#' + hex);
                                    document.styleSheets[0].addRule(currentTagName + ':after', 'content: box-shadow-color:"' + hex + '";');
                                }
                            });
                            jQuery("#colorPickerGlobalStyles").css("background", "#" + hex);
                        }
                    });
                }
            }
        }),
        app.directive('fontSelectedList', function() {
            return {
                restrict: 'A',
                templateUrl: 'elements/font-selected-list.html',
                replace: true,
                controller: function($scope, $http) {
                    var scope = $scope;

                    var fontsURLs = [];
                    var iframeHead;
                    var iframeBody;
                    var mainHead;
                    $http({
                        method: 'GET',
                        url: 'fonts/googleFonts.json'
                    }).then(function(response) {
                        scope.fonts = response.data.fonts;
                        for (fi in scope.fonts) {
                            fontsURLs.push(scope.fonts[fi].link);
                        }
                    });
                    scope.$watch('defaultVariables.font', function(newValue, oldValue) {
                        scope.fonts.unshift(newValue);
                    });
                    scope.$watch('defaultVariables.hasIframeLoaded', function(newValue, oldValue) {
                        if (newValue) {
                            iframeHead = jQuery("#iframeContent").contents().find("head");
                            iframeBody = jQuery("#iframeContent").contents().find("body");
                            mainHead = jQuery('head');
                            /*console.log(fontsURLs);
					console.log(iframeHead);*/
                            iframeHead.append(fontsURLs);
                            mainHead.append(fontsURLs);
                        }
                    });
                    var fontStylesContainer = jQuery('#fonts-list');
                    fontStylesContainer.click(function() {
                        fontStylesContainer.toggleClass('open');
                    });
                    $scope.updateSelectedFont = function(index) {
                        //iframeBody.css('font-family', $scope.fonts[index].font);

                        if (index > 0) {
                            console.log($scope.fonts.shift());
                            $scope.fonts.unshift($scope.fonts[index - 1]);
                            if (iframeHead.find('style.fontsStyle').length > 0) {
                                iframeHead.remove('style.fontsStyle');
                                iframeBody.addClass("change-font");
                                iframeBody.removeClass('change-font');
                            }
                            iframeBody.addClass("change-font");
                            iframeHead.append('<style class="fontsStyle"> .change-font{ font-family:"' + $scope.fonts[index].font + '"!important;}</style>');
                            $scope.selectedFont = $scope.fonts[index];
                        }
                    }

                }
            };
        });

    function SelectText(element) {
        var doc = document,
            text = doc.getElementById(element),
            range, selection;
        if (doc.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(text);
            range.select();
        } else if (window.getSelection) {
            selection = window.getSelection();
            range = document.createRange();
            range.selectNodeContents(text);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    }
    jQuery('#customizer').ready(function() {
        var customizer = jQuery('#customizer');
        var customizerCloseButton = jQuery('#customizer-close-button');
        var customizerOpenButton = jQuery('#customizer-open-button');
        customizerCloseButton.click(function() {
            customizer.toggleClass('open');
            customizerOpenButton.toggleClass('open');
        });
        customizerOpenButton.click(function() {
            customizer.toggleClass('open');
            jQuery(this).toggleClass('open');
        });
        var aboutCustomizerContainer = jQuery('.about-customizer .about-text-container');
        var aboutCustomizerBar = jQuery('.about-customizer .section-title');
        aboutCustomizerBar.click(function() {
            aboutCustomizerContainer.toggleClass('open');
            aboutCustomizerBar.find('.fa').toggleClass('fa-rotate-180');
        });
        var gValues = jQuery('#generatedValues');
        var gCSS = jQuery('#generatedCss');
        gValues.click(function() {
            SelectText(jQuery(this).attr('id'));
        });
        gCSS.click(function() {
            SelectText(jQuery(this).attr('id'));
        })

    });
})();