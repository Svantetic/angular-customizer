# AngularJS Hubspot Customizer for templates
ver: 0.1.0

# co jest zrobione:
## zrobione zostały moduly odpowiedzialne za :
	- szerokosc kontenera(.container)
	- stick header czy belka z menu ma sie przylepiać czy ma zostawać u gory templaty
## została zrobiona kolorystyka menu: 
	- kolor czcionki na wszystkich pozimach menu
	- kolor tła 2 poziomu menu 
	- kolor po hoverze tła 2 poziomu menu 
	- kolor podkreślenia menu 
	- kolor całej belki w ktorej znajduje sie menu
## co jeszcze:
	- został zrobiony generato styli hubowych 
	- został zrobiony globalny kolor , oznacza to ze pobiera i zmienia główny kolor templatki
	- zostaw zrobiony globany font tzn ze mozna zmieniac globalnie fonty dla danej . fonty sa pobiera ajaxem z katalogu fonts z pliku o nazwie googleFonts.json wszystkie fonty sa zapisane w formacie json
	- zostało zrobione pobieranie wszystkich styli z templatki ponieważ tylko tak dało sie dostac do wartosci pseudoelementow i rowniez jest pobierane ajaxem i konwerterowane na jsona zaraz przy ładowaniu iframe
	-cała zawartosc templaty jest w iframie poniewaz jedynie tak mozna uniknac kolizji i nazw  zmiennych funkcji etc.

	- wszystkie opcje zmiany kolorow etc zostały rozbite na dyrektywy i znajdują sie w folderze *elements*
	- javascript zawiera zewnetrzne biblioteki
	- sites zawiera strukture jak templaty maja byc rozmieszczone w jakich katalogach
	- style jest to folder zawierający defaultowa stylistyke do wszystkich elementow (takze iframe);

###WAŻNE
każdą ostatnią regułę dla elementu w cssie należy kończyć średnikiem, ponieważ skrypt parsera CSSTOJSON głupieje i nie przetwarza reguł bez średnika

## co jest jeszcze do zrobienia:

	~~generowanie css buttonów metody są w dyrektywie "buttonsCustomize" w pliku functions.js~~
	~~ możliwość wyboru templaty (pliki sprawdzajace ilosc templat jest w folderze php -> functions.php)
	~~ stylistyka całego customizera
	- !ew małe błedy przy roznych testach