<?php
/*
 * parametry:		dirname i type
 * dirname:			określa katalog, w którym skrypt szuka katalogów/plików
 * type:			określa czego ma szukać:
 *											- directory:	szuka głównych katalogów templat w katalogu Templates/
 *											- templateType: szuka podtypu templaty (email, landing...)
 *											- file:			szuka konkretnego pliku html w podanym podkatalogu templaty np
 * przykładowe requesty i responsy:
 *
 * req:		list-templates.php?dirname=Templates&type=directory
 * res:		{"Sodium":"Templates\/Sodium","Titanium":"Templates\/Titanium","Vanadyl":"Templates\/Vanadyl"}
 * 	
 * req:		list-templates.php?dirname=Sodium&type=templateType
 * res:		{"Email":"\/Templates\/Sodium\/Email","Landing":"\/Templates\/Sodium\/Landing"}	
 *
 * req:		list-templates.php?dirname=Sodium/Email&type=file
 * res:		{"email.html":"email.html"}
 */
function getDirFiles($dir="", $type){
	$json=array();
	if ($type === 'directory'){
	foreach(new DirectoryIterator(dirname(__FILE__)."/../".$dir) as $file){
		if($file->isDot())continue;
		if($file->getFilename()!="html" && $file->getFilename()!="json" && !$file->isDir()) continue;
		$json[$file->getFilename()]=$dir."/".$file->getFilename();		# $json[$file] = $file;
		# $json['type'] = 'you have request directory';
	}}
	elseif ($type === 'templateType') {
		foreach (new DirectoryIterator(dirname(__FILE__)."/../Templates/".$dir) as $file){
		if($file->isDot())continue;
		if($file->getFilename()!="html" && $file->getFilename()!="json" && !$file->isDir()) continue;
		$json[$file->getFilename()]='/Templates/'.$dir.'/'.$file->getFilename();
	}
	}
	# TODO: dodac jakies klucze, aby klucz response'a nie byl nazwa pliku?
	elseif ($type === 'file') {
		foreach (new DirectoryIterator(dirname(__FILE__)."/../Templates/".$dir) as $file){
		if($file->isDot())continue;
		if($file->getFilename()=="html" && $file->getFilename()!="json" && !$file->isDir()) continue;
		$json[$file->getFilename()]=$file->getFilename();
	}}
	echo json_encode($json);
}
if (isset($_GET['dirname']) && isset($_GET['type'])) {
	$dir = $_GET['dirname'];
	$type = $_GET['type'];
	getDirFiles($dir, $type);
}
elseif (isset($_GET['dirname'])){
	echo 'dirnameeeee '.$_GET['dirname'];
}
else {
	echo 'bad request';
}
?>