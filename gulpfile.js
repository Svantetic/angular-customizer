var gulp = require('gulp');
	gutil = require('gulp-util');
  stripDebug = require('gulp-strip-debug');

gulp.task('remove console statements', function() {
  return gulp.src('controllers/*')
    .pipe(stripDebug())
    .pipe(gulp.dest('dist/'));
});